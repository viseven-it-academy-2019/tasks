####Завантаження темплейту в MailChimp:
1. https://us17.admin.mailchimp.com/templates/create-template -> Code your own
2. Ввести в console через dev-tool:
```bash
document.querySelector("#import-zip-file > div > label").setAttribute("onclick", "mojo.app.templates.confirmImportFromZip(); return false;")
```
3. Натиснути Import zip -> завантажити архів, ввести назву шаблона -> save -> Save & Exit

####Додавання акаунтів для відправки:
1. Зберегти набір пошт в csv-файл (`cobalt-test@viseven.com`, `viseven.testing@gmail.com`, `свою пошту`).
2. https://us4.admin.mailchimp.com/lists/members/ Add contacts  -> Import contacts -> Import from CSV file
3. Перейти в додані акаунти та налаштувати `FName`, `LName`

####Відправка листа як Campaign:
1. https://us4.admin.mailchimp.com/campaigns -> Create campaign -> Email
2. Ввести назву розсилки
3. Заповнити дані згідно ТЗ
